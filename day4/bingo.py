#!/usr/bin/env python3
# coding:utf-8
""" Day 4 - Bingo! """
from dataclasses import dataclass
from typing import List

@dataclass
class Number:
    """Number on a bingo board"""
    value: int
    marked: bool

@dataclass
class Board:
    """ Bingo board (aka 'card')"""
    rows: List[List[Number]]
    won: bool

@dataclass
class Game:
    """ Bingo game """
    numbers_called: List[str]
    boards: List[Board]

def parse_bingo_file(filename: str) -> Game:
    """Read commands from file"""
    with open(filename, encoding="utf-8") as file:
        lines = file.readlines()
        numbers = lines.pop(0).strip().split(',') # first line is the numbers called out
        print('numbers called = ' + str(len(numbers)))
        lines.pop(0) # remove first blank line
        current_board = []
        boards = []
        for line in lines:
            if len(line.strip()) != 0:
                nums = line.strip().split()
                num_list = []
                for num in nums:
                    num_list.append(Number(value = int(num), marked = False))
                current_board.append(num_list)
            else:
                boards.append(Board(rows=current_board.copy(), won=False))
                current_board.clear()
        if len(current_board) > 0:
            boards.append(Board(rows=current_board.copy(), won=False))
        print("number of boards = " + str(len(boards)))

    # for board in boards:
    #     # print(board)
    return Game(numbers_called=numbers, boards=boards)

def eval_rows(board: Board) -> bool:
    for row in board.rows:
        not_marked = list(filter(lambda x: (x.marked == False), row))
        if len(not_marked) == 0:
            print("bingo across")
            return True
    return False

def eval_cols(board: Board) -> bool:
    # print()
    cols = [[] for x in range(5)]
    for offset in range(5):
        for row_num in range(5):
            # print(f"board row {row_num} {offset} {board.rows[row_num][offset]}")
            cols[offset].append(board.rows[row_num][offset])
    for col in cols:
        # numbers = map(lambda x: str(x.value), col)
        # print(",".join(numbers))
        not_marked = list(filter(lambda x: (x.marked == False), col))
        if len(not_marked) == 0:
            print("bingo down")
            return True
    return False

def run_game_pt1()-> None:
    game = parse_bingo_file('example_input.txt')
    print(f"game has {len(game.boards)} boards")
    for number_called in game.numbers_called:
        for board in game.boards:
            for row in board.rows:
                for n in row:
                    if n.value == int(number_called):
                        setattr(n, 'marked', True)
        for board in game.boards:
            if eval_rows(board) or eval_cols(board):
                board_sum = 0
                for row in board.rows:
                    for n in row:
                        if not n.marked:
                            board_sum += n.value
                score = board_sum * int(number_called)
                print(f"Pt 1: Score is {board_sum} * {number_called} = {score}")
                return

def run_game_pt2()-> None:
    game = parse_bingo_file('real_input.txt')
    print(f"game has {len(game.boards)} boards")
    for number_called in game.numbers_called:
        for board in game.boards:
            for row in board.rows:
                for n in row:
                    if n.value == int(number_called):
                        setattr(n, 'marked', True)
        for board in game.boards:
            if board.won:
                continue
            if eval_rows(board) or eval_cols(board):
                setattr(board, 'won', True)
                board_sum = 0
                for row in board.rows:
                    for n in row:
                        if not n.marked:
                            board_sum += n.value
                score = board_sum * int(number_called)
                print(f"Pt 2: Score is {board_sum} * {number_called} = {score}")

if __name__ == "__main__":
    run_game_pt1()
    run_game_pt2()
