#!/usr/bin/env python3
# coding:utf-8
""" Day 6 Lantern Fish"""
from typing import List


def read_input(filename: str) -> List[int]:
    """Read input from file"""
    with open(filename, encoding="utf-8") as file:
        return list(map(lambda s: (int(s)), file.readline().strip().split(",")))


def fishes_to_string(fishes: List[int]) -> str:
    return ",".join(map(lambda fish: str(fish), fishes))


fishes = read_input("day6/real_input.txt")
print(fishes)
init_state = fishes_to_string(fishes)
print(f"Initial state: {init_state}")

# for i in range(1, 257):
#     new_fish = []
#     for f in range(len(fishes)):
#         days = fishes[f] - 1
#         if days == -1:
#             new_fish.append(8)
#             days = 6
#         fishes[f] = days
#     fishes.extend(new_fish)
#     new_fish.clear()
#     if i % 50 == 0:
#         milestone = str(i)
#     else:
#         milestone = ""
#     print(f".{milestone}", end="", flush=True)
#     # print(f"After {i} days: {fishes_to_string(fishes)}")

# print("part1 fish count is " + str(len(fishes)))

days = [0] * 9

for fish in fishes:
    days[fish] += 1

for i in range(256):
    today = i % len(days)

    days[(today + 7) % len(days)] += days[today]

print(f"Total after 256: {sum(days)}")
