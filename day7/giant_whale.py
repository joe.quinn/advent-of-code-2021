#!/usr/bin/env python3
# coding:utf-8
""" Day 7 """
from typing import List


def read_input_file(filename: str) -> List[int]:
    """Read positions from file"""
    with open(filename, encoding="utf-8") as file:
        return list(map(int, file.readline().strip().split(",")))


def part1(positions: List[int]):
    """Part 1"""
    fuel_costs = []
    for align_pos in range(0, max(positions) + 1):
        fuel_cost = 0
        for crab in positions:
            fuel_cost += abs(align_pos - crab)
        fuel_costs.append(fuel_cost)

    print(f"Part 1 lowest fuel cost is for position: {min(fuel_costs)}")


def fuel_calc(moves: int) -> int:
    """Calc fuel amount"""
    sum_total = 0
    for i in range(moves + 1):
        sum_total += i
    return sum_total


def part2(positions: List[int]):
    """Part 2"""
    fuel_costs = []
    for align_pos in range(0, max(positions) + 1):
        fuel_cost = 0
        for crab in positions:
            fuel_cost += fuel_calc(abs(align_pos - crab))
        fuel_costs.append(fuel_cost)

    print(f"Part 2 lowest fuel cost is for position: {min(fuel_costs)}")


fuel_positions = read_input_file("day7/real_input.txt")

part1(fuel_positions)
part2(fuel_positions)
