#!/usr/bin/env python3
# coding:utf-8
""" Day 1 """
from typing import List

def measure_depth_increases(measurements: List[int]) -> int:
    """ Measure depth increases between individual measurements (part 1) """
    increases = 0

    for i in range(1, len(measurements)):
        if measurements[i] > measurements[i - 1]:
            increases += 1
    return increases

def measure_window_increases(measurements: List[int]) -> int:
    """ Group measurements into 'windows' before measuring the increases (part 2) """
    windows = []

    for i in range(0, len(measurements) - 2):
        mes1 = measurements[i]
        mes2 = measurements[i + 1]
        mes3 = measurements[i + 2]
        window_sum = mes1 + mes2 + mes3
        windows.append(window_sum)

    return measure_depth_increases(windows)

def read_measurements_file(filename: str) -> List[int]:
    """ Read measurments from a file """
    with open(filename, encoding="utf-8") as file:
        lines = file.read().splitlines()
        return list(map(int, lines))

def get_depth_increases_from_file(filename: str) -> int:
    """ Output the increases between individual measurements in supplied file (part 1) """
    measurements = read_measurements_file(filename)
    return measure_depth_increases(measurements)

def get_window_increases_from_file(filename: str) -> int:
    """ Output the windowed increases from measurments in supplied file (part 2) """
    measurements = read_measurements_file(filename)
    return measure_window_increases(measurements)

def main() -> None:
    """ Main method - output answers to https://adventofcode.com/2021/day/1 """
    print('part 1 answer: ' + str(get_depth_increases_from_file('inputs.txt')))
    print('part 2 answer: ' + str(get_window_increases_from_file('inputs.txt')))

if __name__ == '__main__':
    main()
