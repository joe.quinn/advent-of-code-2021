#!/usr/bin/env python3
# coding:utf-8
""" Tests for Day 2 """
import sonar_sweep

def test_example() -> None:
    """ test part 1 method """
    measurements = [199,
                    200,
                    208,
                    210,
                    200,
                    207,
                    240,
                    269,
                    260,
                    263]
    assert sonar_sweep.measure_depth_increases(measurements) == 7

def test_part1_file() -> None:
    """ test part 2 example """
    assert sonar_sweep.get_depth_increases_from_file('day1/test_inputs.txt') == 7

def test_part2_file() -> None:
    """ test part 2 example """
    assert sonar_sweep.get_window_increases_from_file('day1/test_inputs.txt') == 5
    