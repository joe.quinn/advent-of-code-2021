#!/usr/bin/env python3
# coding:utf-8
""" Tests for Day 1 """
import dive

def test_part1_example() -> None:
    """ Test part 1 """
    assert dive.follow_commands_from_file('./day2/example_commands.txt') == 150

def test_part2_example() -> None:
    """ Test part 2 """
    assert dive.follow_commands_with_aim_from_file('./day2/example_commands.txt') == 900

def test_read_command_file() -> None:
    """ Test reading file """
    cmds = dive.read_command_file('./day2/example_commands.txt')
    assert len(cmds) == 6
    assert cmds[0].direction == 'forward'
    assert cmds[0].amount == 5
