#!/usr/bin/env python3
# coding:utf-8
""" Day 2 """
from typing import List
from dataclasses import dataclass

@dataclass(frozen=True)
class Command:
    """ Command class """
    direction: str
    amount: int

def follow_commands_from_file(filename: str) -> int:
    """ Follow commands from file (part 1) """
    commands = read_command_file(filename)
    return follow_commands(commands)

def follow_commands_with_aim_from_file(filename: str) -> int:
    """ Follow commands from file (part 2) """
    commands = read_command_file(filename)
    return follow_commands_with_aim(commands)

def follow_commands(commands: List[Command]) -> int:
    """ Follow commands (part 1) """
    horizontal = 0
    depth = 0
    for cmd in commands:
        if cmd.direction == 'forward':
            horizontal += cmd.amount
        elif cmd.direction == 'back':
            horizontal -= cmd.amount
        elif cmd.direction == 'down':
            depth += cmd.amount
        elif cmd.direction == 'up':
            depth -= cmd.amount

    print(f'x -> {horizontal} y -> {depth}')
    return horizontal * depth

def follow_commands_with_aim(commands: List[Command]) -> int:
    """ Follow commands but with corrected aim parameter (part 2) """
    aim = 0
    horizontal = 0
    depth = 0
    for cmd in commands:
        if cmd.direction == 'forward':
            horizontal += cmd.amount
            depth += aim * cmd.amount
        elif cmd.direction == 'back':
            horizontal -= cmd.amount
            depth -= aim * cmd.amount
        elif cmd.direction == 'down':
            aim += cmd.amount
        elif cmd.direction == 'up':
            aim -= cmd.amount

    print(f'x -> {horizontal} y -> {depth}')
    return horizontal * depth

def read_command_file(filename: str) -> List[Command]:
    """ Read commands from file """
    commands = []
    with open(filename, encoding="utf-8") as file:
        for line in file:
            (key, val) = line.split()
            commands.append(Command(direction = key, amount = int(val)))

    return commands

def main() -> None:
    """
    Print answers to questions from Day 2
    https://adventofcode.com/2021/day/2
    """
    ans1 = follow_commands_from_file('part1_commands.txt')
    print(f'part 1 answer is: {ans1}')
    ans2 = follow_commands_with_aim_from_file('part1_commands.txt')
    print(f'part 2 answer is: {ans2}')

if __name__ == '__main__':
    main()
  