#!/usr/bin/env python3
# coding:utf-8
""" Day 11 """
from typing import List


def read_input_file(filename: str) -> List[List[int]]:
    """Read commands from file"""
    rows = []
    with open(filename, encoding="utf-8") as file:
        for line in file.readlines():
            rows.append(list(map(int, list(line.strip()))))
    return rows


octopi = read_input_file("day11/example_input.txt")


for o in octopi:
    print(o)
print("---")
# for i in range(100):
for y in range(len(octopi)):
    for x in range(len(octopi[0])):
        octopi[y][x] += 1

for o in octopi:
    print(o)
