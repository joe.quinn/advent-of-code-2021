.PHONY: init
install:
	python3 -m venv .venv
	source .venv/bin/activate
	pip install -r requirements.txt
	pre-commit install
## Lint your code using pylint
.PHONY: lint
lint:
	python -m pylint --version
# python -m pylint day*
# python -m mypy --version
# python -m mypy --disallow-untyped-defs day*
## Run tests using pytest
.PHONY: test
test:
	python -m pytest --version
	python -m pytest
## Run ci part
.PHONY: ci
ci: init lint test
