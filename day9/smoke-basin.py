#!/usr/bin/env python3
# coding:utf-8
""" Day 2 """
from typing import List

from dataclasses import dataclass


@dataclass(frozen=True)
class Point:
    """Coordinate point"""

    height: int
    x: int
    y: int


def read_input_file(filename: str) -> List[List[int]]:
    """Read commands from file"""
    heights = []
    with open(filename, encoding="utf-8") as file:
        for line in file.readlines():
            heights.append(list(map(int, list(line.strip()))))
    return heights


def mark_basin(y: int, x: int, basin: List[Point], heights: List[List[int]]) -> None:

    if int(heights[y][x]) != 9:
        # print("marked")
        print(f"x->{x}, y-> {y} ")
        basin.append(Point(height=heights[y][x], x=x, y=y))
        heights[y][x] = 9  # set to 9 to prevent re-processing


def recurse_basin(low_points: List[Point], heights: List[List[int]]) -> List[Point]:
    new_basin_points: List[Point] = []
    # for row in heights:
    #     print(row)
    for low_point in low_points:
        # print(f"Low point -> {low_point}")
        # need to add point itself here? and set to 9?
        basin: List[Point] = []
        x = low_point.x
        y = low_point.y
        # if x < (len(heights[0]) - 1) and y < (len(heights) - 1):
        #     # print(
        #     #     f"x({x}) len(height[0]) is {len(heights[0])} y({y}) len(height) is {len(heights)}"
        #     # )
        #     mark_basin(y + 1, x + 1, basin, heights)
        # if x < len(heights[0]) - 2 and y > 0:
        #     mark_basin(y - 1, x + 1, basin, heights)
        if x < len(heights[0]) - 1:
            mark_basin(y, x + 1, basin, heights)
        if y > 0:
            mark_basin(y - 1, x, basin, heights)
        if y < len(heights) - 1:
            mark_basin(y + 1, x, basin, heights)
        # if y < len(heights) - 2 and x > 0:
        #     mark_basin(y + 1, x - 1, basin, heights)
        # if y > 0 and x > 0:
        #     mark_basin(y - 1, x - 1, basin, heights)
        if x > 0:
            mark_basin(y, x - 1, basin, heights)

        if len(basin) > 0:
            basin.extend(recurse_basin(basin, heights))
        new_basin_points.extend(basin)
    return new_basin_points


heights = read_input_file("day9/real_input.txt")
print(heights)

low_points: List[Point] = []
for y, height_row in enumerate(heights):
    for x, height in enumerate(height_row):
        surrounding: List[Point] = []

        if x != len(height_row) - 1 and y != len(heights) - 1:
            surrounding.append(Point(height=heights[y + 1][x + 1], x=x + 1, y=y + 1))

        if x != len(height_row) - 1 and y != 0:
            surrounding.append(Point(height=heights[y - 1][x + 1], x=x + 1, y=y + 1))
        if x != len(height_row) - 1:
            surrounding.append(Point(height=heights[y][x + 1], x=x + 1, y=y))
        if y != 0:
            surrounding.append(Point(height=heights[y - 1][x], x=x, y=y - 1))
        if y != len(heights) - 1:
            surrounding.append(Point(height=heights[y + 1][x], x=x, y=y + 1))
        if y != len(heights) - 1 and x != 0:
            surrounding.append(Point(height=heights[y + 1][x - 1], x=x - 1, y=y + 1))
        if y != 0 and x != 0:
            surrounding.append(Point(height=heights[y - 1][x - 1], x=x - 1, y=y - 1))
        if x != 0:
            surrounding.append(Point(height=heights[y][x - 1], x=x - 1, y=y))
        low_point = all(
            i > height for i in list(map(lambda point: point.height, surrounding))
        )
        print(f"x[{x}]y[{y}] {height} {surrounding} {low_point}")
        if low_point:
            low_points.append(Point(height=height, x=x, y=y))
        risk_level = list(map(lambda x: x.height + 1, low_points))
print(
    f"Part 1: {len(low_points)} low points ({low_points}). Sum of risk levels is {sum(risk_level)}"
)
basin_sizes: List[int] = []
for l in low_points:
    print(f"low : {l}")
    basin = recurse_basin([l], heights)
    print(f"basin size-> {len(basin)}")
    basin_sizes.append(len(basin))

highest = sorted(basin_sizes, reverse=True)[:3]
print(f"{highest}")
result = highest[0] * highest[1] * highest[2]
print(f"result={result}")
