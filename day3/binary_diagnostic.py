#!/usr/bin/env python3
# coding:utf-8
""" Day 3 """
from typing import List


def read_report(filename: str) -> List[str]:
    """Read commands from file"""
    with open(filename, encoding="utf-8") as file:
        return file.readlines()


def calculate_power_consumption(report: List[str]) -> int:
    """Calculate power consumption"""
    gamma_rate: List[str] = []
    epsilon_rate: List[str] = []
    for i in range(len(report[0]) - 1):
        binary_zeros = 0
        binary_ones = 0
        for line in report:
            if line[i] == "0":
                binary_zeros += 1
            elif line[i] == "1":
                binary_ones += 1
        if binary_ones > binary_zeros:
            gamma_rate.append("1")
            epsilon_rate.append("0")
        else:
            gamma_rate.append("0")
            epsilon_rate.append("1")
    gamma_rate_dec = int("".join(gamma_rate), 2)
    print(f"gamma_rate {gamma_rate} {gamma_rate_dec}")

    epsilon_rate_dec = int("".join(epsilon_rate), 2)
    print(f"epsilon_rate {epsilon_rate} {epsilon_rate_dec}")

    return gamma_rate_dec * epsilon_rate_dec


def calculate_life_support_rating(report: List[str]) -> int:
    """Calculate Life Support Rating"""
    oxygen_generator_rating = calc_oxygen_generator_rating(report)
    co2_scrubbing_rating = calc_co2_scrubbing_rating(report)

    oxygen_dec = int("".join(oxygen_generator_rating), 2)
    co2_dec = int("".join(co2_scrubbing_rating), 2)
    return oxygen_dec * co2_dec


def calc_oxygen_generator_rating(report: List[str]) -> str:
    """Calculate Oxygen Generator Rating"""
    for i in range(len(report[0]) - 1):
        oxygen_generator_rating_num = ""
        binary_zeros = 0
        binary_ones = 0
        for line in report:
            if line[i] == "0":
                binary_zeros += 1
            elif line[i] == "1":
                binary_ones += 1
        if binary_ones >= binary_zeros:
            oxygen_generator_rating_num = "1"
        else:
            oxygen_generator_rating_num = "0"
        report = list(
            filter(
                lambda line, pos=i, o2=oxygen_generator_rating_num: line[pos] == o2,
                report,
            )
        )
        if len(report) == 1:
            return report[0]
    print(f"warning! >>report length = {len(report)}<<")
    return report[0]


def calc_co2_scrubbing_rating(report: List[str]) -> str:
    """Calculate C02 Scrubbing Rating"""
    for i in range(len(report[0]) - 1):
        co2_scrubbing_rating_num = ""
        binary_zeros = 0
        binary_ones = 0
        for line in report:
            if line[i] == "0":
                binary_zeros += 1
            elif line[i] == "1":
                binary_ones += 1
        if binary_ones >= binary_zeros:
            co2_scrubbing_rating_num = "0"
        else:
            co2_scrubbing_rating_num = "1"
        report = list(
            filter(
                lambda line, i=i, co2=co2_scrubbing_rating_num: line[i] == co2,
                report,
            )
        )
        if len(report) == 1:
            return report[0]
    print(f"warning! >>report length = {len(report)}<<")
    return report[0]


def get_power_consumption(filename: str) -> int:
    """Get Power Consumption"""
    report = read_report(filename)
    return calculate_power_consumption(report)


def get_life_support_rating(filename: str) -> int:
    """Get Life Support Rating"""
    report = read_report(filename)
    return calculate_life_support_rating(report)


def main() -> None:
    """Main"""
    power_consumption = get_power_consumption("day3/report.txt")
    print(f"Power Consumption: {power_consumption}")
    life_support_rating = get_life_support_rating("day3/report.txt")
    print(f"Life Support Rating: {life_support_rating}")


if __name__ == "__main__":
    main()
