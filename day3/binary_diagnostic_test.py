#!/usr/bin/env python3
# coding:utf-8
""" Day 3 Tests"""
import binary_diagnostic


def test_part1():
    """Test part 1"""
    assert binary_diagnostic.get_power_consumption("./day3/example_report.txt") == 198


def test_part2():
    """Test part 2"""
    assert binary_diagnostic.get_life_support_rating("./day3/example_report.txt") == 230
